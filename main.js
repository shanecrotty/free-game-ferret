import axios from "axios";
import * as cheerio from "cheerio";
import { setTimeout } from "timers/promises";

// Asynchronous functions to fetch HTML strings and parse them as 'cheerio' objects.
async function getFreeGames() {
    try {
        const response = await axios.get('https://www.metacritic.com/news/new-free-games-playstation-xbox-pc/');
        const $ = cheerio.load(response.data);

        const listOfGames = [];
        const listOfPrimeGames = [];

        // The first table to be scraped contains free games which do not require a subscription to any other services.
        $('table:first td').each((i, el) => {

            let game = $(el).text().trim();
            if (game.includes('PC')) {

                // Simple regex to omit the platform text (as we are concerned with PC games only)
                game = game.replace(/ PC.*/, '')

                // Fetch the location
                let location = $(el).next().find('a').first().text().trim();
                listOfGames.push({game, location});
            }
        });

        // The eighth table to be scraped contains Prime Gaming info.
        $('table:eq(7):first td').each((i, el) => {

            let game = $(el).find('a').text().trim();
            const location = "Prime Gaming";

            if(game !== 'tbd' && game !== ''){
                listOfPrimeGames.push({game, location});
            }

        });

        return {freeGames: listOfGames, freePrimeGames: listOfPrimeGames};

    } catch (error) {
        console.error('Error fetching free games:', error);
    }
}

console.log("\nGood evening, I am the free games ferret. I shall 'ferret' around for free games on your behalf. \n\n");

console.log("        .^^^~~7?JJJ?!^^~~:                                                                          \n" +
    "        7J?YB###G5YJ!~77?5^                                                                         \n" +
    "        !5!.:7J7^:~~^:~77P!                       ..::^^^^^:.                                       \n" +
    "        ?J!GGGGBBG::PB::75:               ..:^~~!!~~~^^:::^^~!!^.                                   \n" +
    "       !5JJGJ??YPG:7@@G. ~7         .:^~~!!~^^:.              .^7!:                                 \n" +
    "       ~?^:?!77. ~7?5P7  !#PJ!~^^~~!~~^:.                        :77                                \n" +
    "       :7J~?YJ~.^?~^:..^JBBBB#B~...                                ~P:                              \n" +
    "       .. :^!!JBBG555PGBBBBBBBBJ                                   :#B^                             \n" +
    "              .GBBBBBBBBBBBBBBBB!                                  YBBB^                            \n" +
    "               ^BBBBBBBBBBBBBBBBB?                               :YBBB#P                            \n" +
    "                ^BBBBBBBBBBBBBBBBB5~                          ^?5BBBBBB#:                           \n" +
    "                 :5#BBBBBBBBBBBBBBBB5!:                     !PBBBBBBBBB&7                           \n" +
    "                  !&##BBBBBBBBBBBBBBBBG5?!^:.    .......   Y#BBBBBBBBBB&B~                          \n" +
    "                  J#BB##PGBB#BBBBBB##BB###BG5Y?!~~~~^^7BP55#BBBBBBBBBB#BBB7                         \n" +
    "                  7#BBBG...:J#BBBBBP^~~^^::..         .BBB##BBBBBBBB##BBBB#P7:                      \n" +
    "                  :BBB#!    .PBBBB#7               .^!~Y#BB#BBBBBB#BJBBBBBBB#BPJ7~^:.               \n" +
    "               :!77G##Y      :PBBBB:              J##B#B###&BBBB#BY: ~G#BBBBBBBBBBBBGP5Y?7^.        \n" +
    "              .J555YY7    ^5GGG&##J               7??##########BY^    .^?PGBBBBBBBBB######BGJ.      \n" +
    "                          ^J?J???!                   7JJJJJJJJ7:          .:^~~~!!!!~~~^^::..       \n")

getFreeGames().then(results => {
    console.table(results.freeGames.reduce((acc, {game, ...x}) => {acc[game] = x; return acc}, {}));
    console.table(results.freePrimeGames.reduce((acc, {game, ...x}) => {acc[game] = x; return acc}, {}));

    console.log("https://store.epicgames.com/en-US/free-games");
    console.log("https://www.gog.com/partner/free_games?");
    console.log("https://freebies.indiegala.com/");
    console.log("https://store.steampowered.com/search/?category1=998&genre=Free%20to%20Play");
    console.log("https://gaming.amazon.com/home");
})

await setTimeout(20000);

