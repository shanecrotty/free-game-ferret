# free-game-ferret

## Name
Free Game Ferret

## Description
A very simple node script, bundled (via .nexe) into a Windows executable, for the purposes of quickly checking the currently available free games, offered by a number of vendors. List already compiled on metacritic.

## Usage
- To run via the package .exe, simply double-click the "Free Game Ferret.exe".
- To execute in the terminal via the source code, run the "ferret" script on line 3 of the package.json file.

## Authors and acknowledgment
Shane Crotty
